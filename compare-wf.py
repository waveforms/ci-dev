#!/usr/bin/python2
import h5py
import numpy as np
from pycbc.filter import match
from pycbc.results import scatter_histograms
from pycbc.io import FieldArray
from pycbc.types import FrequencySeries
from pycbc.types import TimeSeries
import sys

parameter_keys = [
    "delta_f", "delta_t", "distance", "eccentricity", "eta", "f_max", "f_min",
    "fref", "inc", "mass1", "mass2", "mtot", "phiref", "spin1mag", "spin1phi",
    "spin1theta", "spin1x", "spin1y", "spin1z", "spin2mag", "spin2phi",
    "spin2theta", "spin2x", "spin2y", "spin2z"
]

plot_parameter_keys = [
    "mass1", "mass2", "spin1x", "spin1y", "spin1z", "spin2x", "spin2y",
    "spin2z", "chi_eff", "inc", "phiref", "fref"
]

generation_keys = ["approximant", "generator"]


def all_almost_equal(x, y, threshold=0.0001):
    for i, j in zip(x, y):
        if not almost_equal(i, j):
            return False
    return True


def almost_equal(x, y, threshold=0.0001):
    return abs(x - y) < threshold


def extract_and_verify_h5_metadata(h5_test_file, h5_reference_file,
                                   waveforms_metadata):
    test_metadata = h5_test_file["meta"]
    reference_metadata = h5_test_file["meta"]
    for key in parameter_keys:
        if isinstance(test_metadata[key].value, str):
            assert test_metadata[key].value == reference_metadata[key].value,\
                "The two files represent different simulations and should "\
                + "not be compared"
        elif isinstance(test_metadata[key].value, float):
            # if the waveforms have output float parameters, we can't expect
            # them to be exactly equal
            assert almost_equal(test_metadata[key].value,
                                reference_metadata[key].value, 1.0e-10),\
                "The two files represent different simulations and should "\
                + "not be compared"
        else:
            print(key)
            assert all_almost_equal(test_metadata[key].value,
                                reference_metadata[key].value, 1.0e-10),\
                "The two files represent different simulations and should "\
                + "not be compared"
        waveforms_metadata[key] = test_metadata[key].value
    for key in generation_keys:
        if test_metadata[key].value != reference_metadata[key].value:
            print("Note: attempting cross-comparison between generators or "\
                  "approximants. Expect a looser match.")
            waveforms_metadata[key] = "CrossComparison_" +\
                test_metadata[key].value + "_" +  reference_metadata[key].value
        else:
            waveforms_metadata[key] = test_metadata[key].value


def test_time_domain_mismatch(reference_waveform, test_waveform, delta_t):
    test_sample_points = test_waveform["sample_points"].value
    reference_sample_points = reference_waveform["sample_points"].value
    previous_point = -1
    # check and make sure that the input time series data is
    # allowable for pycbc
    for test_point, reference_point in zip(test_sample_points,\
                                           reference_sample_points):
        assert almost_equal(test_point, reference_point, 1.0e-10),\
            "sample points mismatch found. No meaningful comparison can be made."
        if not previous_point == -1:
            assert almost_equal(test_point - previous_point, delta_t, 1.0e-10),\
                "uneven time steps are not supported by pycbc"
        previous_point = reference_point
    test_hp = TimeSeries(test_waveform["hp"].value, delta_t=delta_t)
    test_hc = TimeSeries(test_waveform["hc"].value, delta_t=delta_t)
    reference_hp = TimeSeries(reference_waveform["hp"].value, delta_t=delta_t)
    reference_hc = TimeSeries(reference_waveform["hc"].value, delta_t=delta_t)
    return abs(1.0 - match(
        test_hp + 1.j * test_hc,
        reference_hp + 1.j * reference_hc,
        low_frequency_cutoff=1,
        high_frequency_cutoff=1024)[0])


def test_frequency_domain_mismatch(reference_waveform, test_waveform, delta_f):
    test_sample_points = test_waveform["sample_points"].value
    reference_sample_points = reference_waveform["sample_points"].value
    previous_point = -1
    # check and make sure that the input time series data is
    # allowable for pycbc
    for test_point, reference_point in zip(test_sample_points,\
                                           reference_sample_points):
        assert almost_equal(test_point, reference_point, 1.0e-10),\
            "sample points mismatch found. No meaningful comparison can be made."
        if not previous_point == -1:
            assert almost_equal(test_point - previous_point, delta_f, 1.0e-10),\
                "uneven time steps are not supported by pycbc"
        previous_point = reference_point
    test_hp = FrequencySeries(test_waveform["hp"].value, delta_f=delta_f)
    test_hc = FrequencySeries(test_waveform["hc"].value, delta_f=delta_f)
    reference_hp = FrequencySeries(
        reference_waveform["hp"].value, delta_f=delta_f)
    reference_hc = FrequencySeries(
        reference_waveform["hc"].value, delta_f=delta_f)
    return abs(1.0 - match(
        test_hp + 1.j * test_hc,
        reference_hp + 1.j * reference_hc,
        low_frequency_cutoff=1,
        high_frequency_cutoff=1024)[0])


def generate_scatterplot(waveform_metadata, mismatches, filename):
    plot_data = FieldArray(
        len(mismatches),
        dtype=[("mass1", float), ("mass2", float), ("spin1x", float),
               ("spin1y", float), ("spin1z", float), ("spin2x", float),
               ("spin2y", float), ("spin2z", float), ("chi_eff", float),
               ("inc", float), ("phiref", float), ("fref", float)])
    for key in plot_parameter_keys:
        plot_data[key] = waveform_metadata[key]
    min_mismatch = np.min(mismatches)
    max_mismatch = np.max(mismatches)
    fig, axes_dict = scatter_histograms.create_multidim_plot(
        plot_parameter_keys,
        plot_data,
        show_colorbar=True,
        zvals=mismatches,
        plot_contours=False,
        scatter_cmap='viridis_r',
        plot_marginal=False,
        vmin=min_mismatch,
        vmax=max_mismatch)
    fig.suptitle("mismatch of " + waveform_metadata["generator"] +
                 " for approximant " + waveform_metadata["approximant"] +
                 ".\n Worst mismatch: {0}".format(max_mismatch))
    fig.savefig(filename)


def main():
    if len(sys.argv) != 3 and len(sys.argv) != 4:
        print("usage: compare-wf.py waveform1 waveform2 [threshold]")

    threshold = 1.0e-8
    if len(sys.argv) == 4:
        threshold = float(sys.argv[3])

    with h5py.File(sys.argv[1], 'r') as test_h5_waveform_file,\
         h5py.File(sys.argv[2], 'r') as reference_h5_waveform_file :
        waveforms_metadata = dict()
        try:
            number_of_waveforms = len(reference_h5_waveform_file["wfs"].keys())
            assert number_of_waveforms ==\
                len(reference_h5_waveform_file["wfs"].keys()),\
                "number of waveforms is mismatched between input files"
            extract_and_verify_h5_metadata(test_h5_waveform_file,
                                           reference_h5_waveform_file,
                                           waveforms_metadata)
        except (AttributeError):
            print >>sys.stderr, \
                "one of the h5 files was passed in an unexpected format."
            raise
        waveforms_metadata["chi_eff"] = \
            (waveforms_metadata["mass1"] * waveforms_metadata["spin1z"] +
             waveforms_metadata["mass2"] * waveforms_metadata["spin2z"]) / \
            waveforms_metadata["mtot"]

        assert all_almost_equal(waveforms_metadata["mass1"]
                                + waveforms_metadata["mass2"],
                                waveforms_metadata["mtot"], 1.0e-10),\
                "Malformed metadata: total mass "\
                + "is not the sum of the component masses"
        assert all_almost_equal(waveforms_metadata["mass1"]
                                * waveforms_metadata["mass2"] /
                                waveforms_metadata["mtot"]**2,
                                waveforms_metadata["eta"], 1.0e-10),\
                            "Malformed metadata: eta is not derivable " +\
                            "from the component masses"
        is_time_domain = True
        if "FD" in waveforms_metadata["generator"]:
            is_time_domain = False
            if "TD" in waveforms_metadata["generator"]:
                print("Error: attempted cross-comparison between a frequency "
                      "and a time domain output, which will produce a "
                      "nonsensical comparison")
        elif "TD" not in waveforms_metadata["generator"]:
            print >>sys.stderr,\
                "Error: unknown generator type"
            raise Exception('unknown_generator')
        mismatches = np.asarray([])
        is_cross_comparison = "CrossComparison" in \
            waveforms_metadata["generator"] \
            or "CrossComparison" in waveforms_metadata["approximant"]
        all_passed = True
        for key in reference_h5_waveform_file["wfs"].keys():
            reference_waveform = reference_h5_waveform_file["wfs"][key]
            test_waveform = test_h5_waveform_file["wfs"][key]
            if is_time_domain:
                mismatches = np.append(
                    mismatches,
                    test_time_domain_mismatch(reference_waveform,
                                              test_waveform,
                                              waveforms_metadata["delta_t"]))
            else:
                mismatches = np.append(
                    mismatches,
                    test_frequency_domain_mismatch(
                        reference_waveform, test_waveform,
                        waveforms_metadata["delta_f"]))
            if almost_equal(mismatches[-1], 0, threshold):
                print("Waveform " + key + " **Passed** ")
            else:
                print("Waveform " + key + " **Failed** ")
                all_passed = False
            for key in parameter_keys:
                if isinstance(waveforms_metadata[key], str):
                    print(key + ": " + waveforms_metadata[key])
                elif isinstance(waveforms_metadata[key], float):
                    print(key + ": " + str(waveforms_metadata[key]))
                else:
                    print(key + ": " +
                          str(waveforms_metadata[key][len(mismatches) - 1]))
        generate_scatterplot(
            waveforms_metadata, mismatches,
            "Comparison_plot_" + sys.argv[1].split("/")[-1].split(".")[0] +
            "_vs_" + sys.argv[2].split("/")[-1].split(".")[1])
        assert all_passed, "Some waveforms have failed for this dataset."


if __name__ == "__main__":
    main()
