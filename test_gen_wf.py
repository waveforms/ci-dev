import pytest

import lalsimulation as lalsim

from wavereview.waveform import get_lalsim_waveform
import wavereview.parameters

def get_lalsim_fd_approximants():
    res=[]
    for approx_num in xrange(0, lalsim.NumApproximants):
        if lalsim.SimInspiralImplementedFDApproximants(approx_num):
            res.append(lalsim.GetStringFromApproximant(approx_num))
    return res

def get_lalsim_td_approximants():
    res=[]
    for approx_num in xrange(0, lalsim.NumApproximants):
        if lalsim.SimInspiralImplementedTDApproximants(approx_num):
            res.append(lalsim.GetStringFromApproximant(approx_num))
    return res

td_keys=('mass1', 'mass2', 'approximant')
fd_keys=('mass1', 'mass2', 'approximant')

skip_td_wfs = ['PhenSpinTaylor', 'PhenSpinTaylorRD','SEOBNRv4T', 'SEOBNRv2T', 'EccentricTD','NRSur7dq2','NR_hdf5','TEOBResum_ROM','SpinDominatedWf']

skip_fd_wfs = ['NRSur4d2s']

td_vals=[]
for td_apx in get_lalsim_td_approximants():
    if td_apx in skip_td_wfs:
        continue
    elif 'ROM' in td_apx:
        continue
    td_vals.append( (50, 40, td_apx) )

fd_vals=[]
for fd_apx in get_lalsim_fd_approximants():
    print(fd_apx)
    if fd_apx in skip_fd_wfs:
        continue
    elif 'ROM' in fd_apx:
        continue
    fd_vals.append( (50, 40, fd_apx) )


@pytest.mark.parametrize(td_keys, td_vals)
def test_SimInspiralChooseTDWaveform(mass1, mass2, approximant):
    generator_function = lalsim.SimInspiralChooseTDWaveform
    Pars = wavereview.parameters.Parameters(domain='td')
    Pars.params.update({"mass1":mass1, "mass2":mass2, "approximant":approximant})
    hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)
    pass

both = set(td_vals).union(set(fd_vals))
# @pytest.mark.parametrize(td_keys, td_vals)
@pytest.mark.parametrize(td_keys, both)
def test_SimInspiralTD(mass1, mass2, approximant):
    generator_function = lalsim.SimInspiralTD
    Pars = wavereview.parameters.Parameters(domain='td')
    Pars.params.update({"mass1":mass1, "mass2":mass2, "approximant":approximant})
    hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)
    pass

@pytest.mark.parametrize(fd_keys, fd_vals)
def test_SimInspiralChooseFDWaveform(mass1, mass2, approximant):
    generator_function = lalsim.SimInspiralChooseFDWaveform
    Pars = wavereview.parameters.Parameters(domain='fd')
    Pars.params.update({"mass1":mass1, "mass2":mass2, "approximant":approximant})
    hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)
    pass


# @pytest.mark.parametrize(fd_keys, fd_vals)
both = set(td_vals).union(set(fd_vals))
@pytest.mark.parametrize(fd_keys, both)
def test_SimInspiralFD(mass1, mass2, approximant):
    generator_function = lalsim.SimInspiralFD
    Pars = wavereview.parameters.Parameters(domain='fd')
    Pars.params.update({"mass1":mass1, "mass2":mass2, "approximant":approximant})
    hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)
    pass

