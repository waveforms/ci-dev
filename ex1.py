import lal
print("this is a test")
print("lal.C_SI = {}".format(lal.C_SI))


import wavereview
print(wavereview.__file__)




import copy

import lal
import lalsimulation as lalsim

from wavereview.waveform import get_lalsim_waveform
import wavereview.parameters



def almost_equal(x,y,threshold=0.0001):
  return abs(x-y) < threshold



def test_swapping_constituents_non_spin(approx="EOBNRv2HM"):
    """
    for non-spinning waveforms if you swap the masses and spins
    you should get the same waveform
    """

    generator_function = lalsim.SimInspiralChooseTDWaveform

    Pars = wavereview.parameters.Parameters(domain='td')


    #FIXME: add phiRef to this?

    Pars.params.update({
        "mass1":50,
        "mass2":30,
        "approximant":approx
    })

    hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)

    params_swap = copy.copy(Pars.params)
    params_swap.update({
        "mass1":Pars.params["mass2"],
        "mass2":Pars.params["mass1"],
        "approximant":approx
    })

    hp_swap, hc_swap = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **params_swap)


print("running test")
test_swapping_constituents_non_spin()

