

import lalsimulation as lalsim
import numpy as np
from wavereview.waveform import get_lalsim_waveform
import wavereview.parameters



generator_function = lalsim.SimInspiralChooseTDWaveform

Pars = wavereview.parameters.Parameters(domain='td')


approx = "SEOBNRv4"

Pars.params.update({
    "mass1":50,
    "mass2":30,
    "approximant":approx
})

hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)
sample_points = np.arange(hp.data.length) * hp.deltaT

import h5py

f = h5py.File('ref-wf.h5', 'w')
h5_wf = f.create_group("wf")
h5_wf.create_dataset("sample_points", data=sample_points)
h5_wf.create_dataset("hp", data=hp.data.data)
h5_wf.create_dataset("hc", data=hc.data.data)
f.close()
