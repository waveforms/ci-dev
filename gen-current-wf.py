

import lalsimulation as lalsim
import numpy as np
from wavereview.waveform import get_lalsim_waveform
import wavereview.parameters



generator_function = lalsim.SimInspiralChooseTDWaveform

Pars = wavereview.parameters.Parameters(domain='td')


approx = "SEOBNRv4"

Pars.params.update({
    "mass1":50,
    "mass2":30,
    "approximant":approx
})

hp, hc = get_lalsim_waveform(generator_function, cast_as_pycbc=True, **Pars.params)

import h5py

f = h5py.File('curr-wf.h5', 'w')
h5_wf = f.create_group("wf")
h5_wf.create_dataset("sample_points", data=hp.sample_times.numpy())
h5_wf.create_dataset("hp", data=hp.numpy()*1e-3)
h5_wf.create_dataset("hc", data=hc.numpy()*1e-3)
f.close()
